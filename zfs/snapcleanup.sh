#!/usr/bin/bash
PATH="/usr/bin:/bin:/usr/sbin:/sbin"
# retention variables
ZPOOL='data'
SECSTOKEEP=604800
# SECSTOKEEP=1296000 # 2592000 = 30 days, 1296000 = 15 days, 604800 = 7 days, 259200 = 3 days
NOW=`/bin/date +%s`
#[ $? -ne 0 ]; echo "No date, exiting"; exit 1
LZFS="/usr/sbin/zfs"
LZPOOL="/usr/sbin/zpool"

replconfirmed="replication:confirmed"
repldepend="replication:depend"

slack_say() {
    message=$1
    severity=$2
    ~/slack_say "$message" "#inf_backups" "$severity"
}

for POOL in $ZPOOL; do
   echo $POOL
   echo

   if [ `$LZPOOL list | grep -c "^$POOL"` -gt 0 ]; then

      # check if zpool is scrubbing
      [ `$LZPOOL status -v $POOL | grep -c 'scrub in progress'` -gt 0 ] && \
         echo Scrub in progress on $POOL, stop && \
         echo Abort $POOL && \
         continue

      # print snapshot info
      echo Snapshot statistics
      $LZFS get -rHp used $($LZPOOL list -H -o name $POOL ) |\
      nawk '/@/ && $2 == "used" { tot++; total_space+=$3 ;\
         if ( $3 == 0 ) { empty++ }} \
      END { printf("%d snapshots\n%d empty snapshots\n%2.2f G in %d snapshots\n", tot, \
         empty, total_space/(1024^3), tot - empty ) }'
      echo

      # clean up old snapshots
      echo Cleaning up snapshots older than $SECSTOKEEP seconds.
      DEPRICATED=0
      for fs in `$LZFS list -t snapshot -o name | awk '!/NAME/ && !/no datasets availible/'`; do
         CREATED=`$LZFS get -Hpo value creation $fs`
         DELTA=$((NOW - CREATED))
         replconfirmcheck=`$LZFS get -H $replconfirmed $fs | cut -f3`
         repldependcheck=`$LZFS get -H $repldepend $fs | cut -f3`
         if [ $DELTA -gt $SECSTOKEEP ]; then
            #echo $fs was created at $CREATED which was $DELTA seconds ago.
            #if [[ $repldependcheck = "false" && $replconfirmcheck = "true" ]]; then
                echo Depend = $repldependcheck : Replicated = $replconfirmcheck, Destroying
                echo "$LZFS destroy $fs"
                $LZFS destroy $fs
                if [ $? -ne 0 ]; then
                    echo "Failed deleting snapshot"
                    exit 1
                fi
                let "DEPRICATED += 1"
            #fi
         fi
      done
      echo Done cleaning up snapshots. $DEPRICATED snapshots removed.
      #slack_say "[$hostname] Snapshot Cleanup removed $DEPRICATED snapshots" Information
      echo

      # print snapshot info
      echo Snapshot statistics
      $LZFS get -rHp used $($LZPOOL list -H -o name $POOL ) |\
      nawk '/@/ && $2 == "used" { tot++; total_space+=$3 ; if ( $3 == 0 ) { empty++ }} END { printf("%d snapshots\n%d empty snapshots\n%2.2f G in %d snapshots\n", tot, empty, total_space/(1024^3), tot - empty) }'
   else
      echo zpool $POOL not found, skipping
   fi

   echo
   echo Finished $POOL
   echo
done
