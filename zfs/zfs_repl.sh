function zfs_repl() {
  if [[ $EUID -eq 0 ]] || [[ $SUDO_USER ]]; then
    echo "please be yourself"
    exit 1
  fi
  [ -z $1 ] && echo "argument must be supplied" && return
  
  if [[ $(zfs get -H mounted $1 |awk '{print $3}') != "yes" ]]; then
    echo "$1 is not mounted or not a zfs.  try again"
    exit 1
  fi
  if [[ $(zfs get -H encryption $1|awk '{print $3}') == "off" ]]; then
    echo "please encrypt $1 with zfs_eatr.sh"
    exit
  fi
  
  [[ $(zfs get -H replication:dsthost $1| awk '{print $3}') == "-" ]] && setup_source $1
  
  dsthost=$(zfs get -H replication:dsthost $1 | awk '{print $3}')
  echo "dsthost $dsthost"
  dstpool=$(zfs get -H replication:dstpool $1 | awk '{print $3}')
  echo "dstpool $dstpool"
  zfs=${1##*/}
  echo "zfs $zfs"
  target=$(ssh $dsthost "/sbin/zfs list -H ${dstpool}/${zfs} 2>/dev/null| awk '{print \$1}'|head -1")
  #
  # check permissions on dsthost
  if [[ ! $(ssh $dsthost /sbin/zfs allow data|grep zfsrepl) =~ "keylocation" ]]; then
    echo "ZFS permissions for replication need to be set on $dsthost"
    ssh $dsthost "sudo /sbin/zfs allow -u zfsrepl load-key,change-key,keylocation data"
  fi
  # if remote target is found and unencrypted, preserve
  if [[ $target ]]; then
    echo "target $target"
    crypted=$(ssh $dsthost "/sbin/zfs get -H encryption $target"|awk '{print $3}')
    case $crypted in
      off)
        echo "nonencrypted replication on $dsthost at $target"
        echo "renaming to ${target}_backup"
        ssh $dsthost "sudo /sbin/zfs rename $target ${target}_backup"
        ;;
      *)
        echo "replication target $dsthost $target is encrypted"
        echo "$crypted"
        echo "our work is done"
        exit
        ;;
    esac
  fi
  # create dirs 
  #create_target $dsthost $1
  #create_target $dsthost ${1}/remotes
  #create_target $dsthost ${1}/remotes/$(hostname -s)
  # create a snapshot and send it
  # cleanup snapshots
  snappy=$(zfs list -t snapshot | grep ${1}@ | awk '{print $1}')
  if [[ ! -z $snappy ]]; then
    while read -r snap; do echo "deleting $snap"; sudo zfs destroy -r $snap; done <<<"$snappy"
  fi
  sudo zfs snapshot ${1}@base
  sudo su - zfsrepl -c "zfs send -cwvP ${1}@base | ssh $dsthost /sbin/zfs recv -vFe -o keylocation='file:///var/adm/crypt' $dstpool"
  sudo su - zfsrepl -c "ssh $dsthost /sbin/zfs load-key -r $dstpool"
  ssh $dsthost "sudo /sbin/zfs mount -a"
}

setup_source() {
  # make a bunch of assumptions
  echo "setup $1 for replication"
  case $(hostname -s) in
    pdxprstore02)
      dsthost=seaprstore02
      ;;
    pdxprstore03)
      dsthost=seaprstore03
      ;;
    seaprstore02)
      dsthost=pdxprstore02
      ;;
    seaprstore03)
      dsthost=pdxprstore03
      ;;
    *)
      echo "I don't know what to do with $(hostname -s)"
      exit 999
  esac
  # 
  sudo zfs set replication:hostid=$(hostname -s) $1
  sudo zfs set replication:dsthost=$dsthost $1
  sudo zfs set replication:dstpool=data/remotes/$(hostname -s) $1
  sudo zfs set replication:frequency=3600 $1
  zfs get -H replication:hostid $1
  zfs get -H replication:dsthost $1
  zfs get -H replication:dstpool $1
  zfs get -H replication:frequency $1
}

create_target() {
  # creates $dir on $dsthost if its not there
  dsthost=$1
  dir=$2
  if [[ $(ssh $dsthost "/sbin/zfs get -H mounted $dir 2>/dev/null"|awk '{print $3}') != "yes" ]]; then
    echo "creating $dir on $dsthost"
    ssh $dsthost "sudo /sbin/zfs create -o encryption=on -o keyformat=passphrase -o keylocation=file:///var/adm/crypt $dir"
  else
    echo "$dir exists on $dsthost"
  fi
}

zfs_repl $@
