# ZFS replication and encryption #

talk about how it works

## replication tooling ##
- newrepl.sh - performs the replication
- snapcleanup.sh - cleans up afterwards
- zfs_set_perms.sh - setup all the perms on a new system
- zfs_eatr.sh - older function used to convert volumes to encryption

### typical crontab ###
```
[zfsrepl@pdxprstore02 ~]$ crontab -l
# HEADER: This file was autogenerated at 2020-05-27 22:25:35 -0700 by puppet.
# HEADER: While it can still be managed manually, it is definitely not recommended.
# HEADER: Note particularly that the comments starting with 'Puppet Name' should
# HEADER: not be deleted, as doing so could cause duplicate cron jobs.
# Puppet Name: zfs-replication-job
0 */1 * * * /bin/bash /home/zfsrepl/newrepl.sh > /dev/null 2>&1 &
# Puppet Name: zfs-replication-cleanup
0 */12 * * * /bin/bash /home/zfsrepl/snapcleanup.sh > /dev/null 2>&1 &
```
