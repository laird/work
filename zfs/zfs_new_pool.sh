#!/bin/bash

function new_pool() {
  if [[ $EUID -eq 0 ]] || [[ $SUDO_USER ]]; then
    echo "please be yourself"
    exit 1
  fi
  [ -z $1 ] && echo "argument must be supplied" && return
  #new_pool=${1##data/}
  new_pool=$1
  sudo zfs create -o encryption=on -o keyformat=passphrase -o keylocation=file:///var/adm/crypt $new_pool || { 
    echo "zfs create of $new_pool failed" ; exit;  }
  # set replication dsthost
  dsthost=$(echo $HOSTNAME| sed 's/pdx/s_ea/; s/sea/pdx/; s/s_ea/sea/'); echo replication dsthost : $dsthost
  #
  # prepare the source pool for replication
  sudo zfs set replication:hostid=`hostname -s` $new_pool
  sudo zfs set replication:dsthost=$dsthost $new_pool
  sudo zfs set replication:dstpool=data/remotes/$(hostname -s) $new_pool
  sudo zfs set replication:frequency=3600 $new_pool
  sudo zfs snapshot ${new_pool}@base
  show_pool $new_pool
  #
  echo
  banner "send the initial replication snapshot to $dsthost"
  #check_dsthost $new_pool
  dstpool="data/remotes/$(hostname -s)/${new_pool##data/}"
  echo dstpool $dstpool
  sudo su - zfsrepl -c "zfs send -cwvP ${new_pool}@base | ssh $dsthost \"/sbin/zfs recv -vFe -o keylocation='file:///var/adm/crypt' data/remotes/$(hostname -s)\""
  sudo su - zfsrepl -c "ssh $dsthost \"/sbin/zfs load-key -r $dstpool\""
  #sudo su - zfsrepl -c "ssh $dsthost \"sudo /sbin/zfs mount -a\""
  check_remote $new_pool
}

function banner() {
  echo 
  echo -e $@
  echo --------------------------------------------------------------------------
}

function show_pool() {
  banner "$1 pool details"
  zfs get -H replication:hostid $1
  zfs get -H replication:dsthost $1
  zfs get -H replication:dstpool $1
  zfs get -H replication:frequency $1
}
#
function check_dsthost() {
# inspects dsthost and bombs if something is already there or 
  [ -z $1 ] && echo "argument must be supplied" && return
  dsthost=$(echo $HOSTNAME| sed 's/pdx/s_ea/; s/sea/pdx/; s/s_ea/sea/'); echo replication dsthost : $dsthost
  result=$(sudo su - zfsrepl -c "ssh $dsthost \"/sbin/zfs list | grep $1\"")
  if [[ ! -z $result ]]; then
    echo $result
    echo "replication dsthost for $new_pool already exists on $dsthost"
    exit
  fi
} 

function check_remote() {
  [ -z $1 ] && echo "argument must be supplied" && return
  remote_pool=${1##data/}
  dsthost=$(echo $HOSTNAME| sed 's/pdx/s_ea/; s/sea/pdx/; s/s_ea/sea/'); echo replication dsthost : $dsthost
  dstpool=data/remotes/$(hostname -s)/${new_pool##data/}
  result=$(sudo su - zfsrepl -c "ssh $dsthost \"/sbin/zfs list | grep $remote_pool\"")
  if [[ -z $result ]]; then
    echo "$1 not found on $dsthost"
    exit
  else
    banner "replication dsthost $1 on $dsthost $dstpool"
    echo $result
    sudo su - zfsrepl -c "ssh $dsthost \"/sbin/zfs get all -H $dstpool | grep -e key -e mount\"" 
    #sudo su - zfsrepl -c "ssh $dsthost \"for hostid dsthost dstpool frequenc
  fi
}
  

echo 
new_pool $@
echo
