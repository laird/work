function zfs_eatr() {
  [ -z $1 ] && echo "argument must be supplied" && return
  [[ $(grep $1 /etc/exports) ]] &&  echo "please remove $1 from /etc/exports and exportfs -av" && exit 1
  snappy=$(zfs list -t snapshot | grep $1 | awk '{print $1}')
  if [[ ! -z $snappy ]]; then
    echo "$1 has snapshots that need to be cleaned up before encryption."
    while read -r snap; do 
      echo "deleting $snap"
      zfs destroy -r $snap
    done <<<"$snappy"
  fi
  zfs_new=${1}_new
  [[ ! -z $(zfs list | grep $zfs_new) ]] && echo "$zfs_new already exists" && return
  zfs_tmp=${1}_tmp
  [[ ! -z $(zfs list | grep $zfs_tmp) ]] && echo "$zfs_tmp already exists" && return
  snap1=${1}@snap1
  [[ ! -z $(zfs list -t snapshot | grep $snap1) ]] && echo "$snap1 already exists" && return
  zfs create -o encryption=on -o keyformat=passphrase -o keylocation=file:///var/adm/crypt $zfs_tmp
  zfs snapshot $snap1
  echo "sending first snapshot"
  zfs send -Rv $snap1 | zfs recv -vFe -o encryption=on -o keyformat=passphrase -o keylocation=file:///var/adm/crypt $zfs_tmp
  snap2=${1}@snap2
  zfs snapshot $snap2
  echo "sending second snapshot"  
  zfs send -Rv -i $snap1 $snap2 | zfs recv -Fe -v $zfs_tmp
  zfs rename $1 ${1}_old
  vol=$(ls /$zfs_tmp)
  zfs_received=$zfs_tmp/$vol
  #echo "ZFS_RECEIVED $zfs_received"  
  zfs rename $zfs_received $1
  [[ $? -eq 0 ]] && zfs destroy -r $zfs_tmp
  for snap in $(zfs list -t snapshot $1 | grep -e snap1 -e snap2| awk '{print $1}'); do echo removing $snap; zfs destroy $snap; done
  # update replication properties
  if [[ $(zfs get -H replication:hostid $1| awk '{print $3}') == $(hostname -s) ]]; then
    hostid=$(zfs get -H replication:hostid $1| awk '{print $3}')
    zfs set replication:hostid=$hostid $1
    dsthost=$(zfs get -H replication:dsthost $1| awk '{print $3}')
    zfs set replication:dsthost=$dsthost $1
    dstpool=$(zfs get -H replication:dstpool $1| awk '{print $3}')
    zfs set replication:dstpool=$dstpool $1
    frequency=$(zfs get -H replication:frequency $1| awk '{print $3}')
    zfs set replication:frequency=$frequency $1
  fi
    
}

zfs_eatr $@
