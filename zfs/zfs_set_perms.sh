function zfs_set_perms() {
  # this function sets all the typical permissions needed

  target="${1:-data}" # sets good default
  user="${2:-zfsrepl}" # sets good default
  
  perms="userprop,send,snapshot,receive,promote,mount,destroy,create,hold,load-key,change-key,keylocation"

  if [[ $EUID -eq 0 ]] || [[ $SUDO_USER ]]; then
    echo "please be yourself"
    exit 1
  fi
  echo 
  echo "before changes"
  echo
  zfs allow $target
  sudo /sbin/zfs allow -u $user $perms $target
  echo
  echo "after changes"
  echo 
  zfs allow $target
  echo 
}

zfs_set_perms "$@"
