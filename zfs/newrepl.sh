#!/usr/bin/bash -p
#
# Instructions for use:
#
# Create a ZFS volume under a parent pool to setup replications parameters
#
# Set the "owner" Host ID, this defines the owner or source the volume originates from
# This will prevent a possible loop, if we attempt to replicate a volume back to ourselves
# `zfs set replication:hostid=`hostname -s` poolname/fsname`
#
# Set a target FQDN for the replication to send to
# `sudo zfs set replication:dsthost=some.fqdn.com poolname/fsname`
#
# Set the target pool/filesystem) to receive the snapshots into (ie. pool0/remotes/sourcehostname)
# `sudo zfs set replication:dstpool=pool0/remotes/sourcehostname
#
# Set the frequency in seconds that snaps and replication should occur at
# `sudo zfs set replication:frequency=3600 pool/fsname`
#
# A base snapshot must be performed first and seeded to the remote end
#
# sudo zfs snapshot pool/filesystem@base
# sudo zfs send -cpP pool/filesystem@base | ssh user@host /sbin/zfs recv -vFe pool0/remotes/sourcehostname


# Some vars
zfs=/usr/sbin/zfs
local_hostid=`/bin/hostname -s`

# Establish a ZFS user property to store replication settings/locking
#
# repl_lock : Set a flag to indicate a lock, or work in progress
# repl_confirmed: Set a flag when remote end has confirmed receipt of the snapshot
# snapshot_time: Time in seconds (epoch) the snap was taken, applied to volume, and the child snapshot
# snap_frequency: Time in seconds to set desired interval to take snaps
# repl_dsthost: Hostname of the destination target to receive snapshot stream
# repl_dstpool: Destination parent ZFS pool/filesystem path to place the received snapshot (use with zfs recv -e to strip leading parent path)
# repl_hostid: Source hostname of the sender, used to prevent replication loops

repl_lock="replication:locked"
repl_confirmed="replication:confirmed"
snapshot_time="replication:epoch"
snap_frequency="replication:frequency"
repl_dsthost="replication:dsthost"
repl_dstpool="replication:dstpool"
repl_hostid="replication:hostid"

slack_say() {
    message=$1
    severity=$2
    ~/slack_say "$message" "#inf_backups" "$severity"
}

main()
{
    # List of file systems to iterate, omit the root pool (1st line) and then iterate for N
    localvolumes=`$zfs list -Ht filesystem | /usr/bin/sed '1d' | /usr/bin/cut -f1`
    #[ $? -eq 1 ]; echo $localvolumes; exit 1
    while IFS= read -r filesystem ; do check_hostid; done <<< "$localvolumes"
}


check_hostid()
{
    # Check to see if "owner" of the filesystem is our local short hostname. Otherwise the filesystem
    # came from elsewhere and we do not want replication loops
    #filesystem_hostid=`$zfs get -H $repl_hostid $filesystem | /usr/bin/cut -f3`
    #if [ $filesystem_hostid = $local_hostid ]; then
    #    check_lock
    #else
    #    echo "I'm not owner of $filesystem"
    #fi

    filesystem_hostid="$($zfs get -s local -H $repl_hostid $filesystem | /usr/bin/cut -f3 | /usr/bin/grep $local_hostid)"
    if [ $? -eq 0 ]; then
        check_lock
    else
        echo "Not owner of $filesystem, skipping"
        :
    fi

}


check_lock()
{
    # Check the local and remote filesystems for locks from other jobs so we don't run multiples
    localfslocked=`$zfs get -s local -H $repl_lock $filesystem | /usr/bin/cut -f3`
    if [[ $localfslocked = "true" || $localfslocked = "True" ]];then
        echo "Filesystem locked, skipping: $filesystem"
        slack_say "[$local_hostid] Filesystem locked, skipping: $filesystem" Information
    else
        check_snapdelta
    fi
}


check_snapdelta()
{
    now_epoch=`/usr/bin/date +%s`
    last_epoch=`$zfs get -s local -H $snapshot_time $filesystem | /usr/bin/cut -f3`
    frequency=`$zfs get -s local -H $snap_frequency $filesystem | /usr/bin/cut -f3`

    echo "$filesystem"
    echo "$last_epoch"
    echo "$frequency"
    if [ -z "$last_epoch" ];then
        echo "No snap epoch found on $filesystem, forcefully taking snapshot now"
        take_snap
    else
        delta=$(($now_epoch - $last_epoch))
        if [ $delta -gt $frequency ]; then
            echo "$filesystem is candidate for snapshot, delta: $delta seconds"
            #slack_say "[$local_hostid] $filesystem is candidate for snapshot, delta: $delta seconds" Information
            take_snap
        else
            echo "$filesystem snap delta ($delta seconds) is not greater than $frequency seconds"
            #slack_say "[$local_hostid] $filesystem snap delta ($delta seconds) is not greater than $frequency seconds" Information
            :
        fi
    fi
}


take_snap()
{
    # Get a good date to assign the snapshot name
    snap_date=`/usr/bin/date +%Y%m%d-%H:%M:%S`
    snap_epoch=`/usr/bin/date +%s`
    snap_name="$filesystem@$snap_date"

    # echo "Setting lock on $filesystem"
    $zfs set $repl_lock=true $filesystem
    # Get name of prior snapshot for use later
    localprevsnap=`$zfs list -Hr -d 1 -o name -s creation -t snapshot $filesystem | /usr/bin/tail -1`

    # Take the snapshot
    $zfs snapshot $snap_name

    if [ $? -ne 0 ]; then
        echo "Snapshot failed for $filesystem, unlocking and exiting"
        slack_say "[$local_hostid] Snapshot failed for $filesystem, unlocking and exiting" Warning
        $zfs set $repl_lock=false $filesystem
    else
        $zfs set $snapshot_time=$snap_epoch $snap_name
        $zfs set $snapshot_time=$snap_epoch $filesystem
        $zfs set $repl_confirmed=false $snap_name
        #slack_say "[$local_hostid] Snapshot for $filesystem completed at $snap_date" Information
        snap_repl
    fi
}


snap_repl()
{
    # Check to see if volume has a target defined, if not, can skip and unlock volume
    host_target=`$zfs get -H $repl_dsthost $filesystem | /usr/bin/cut -f3`
    if [ $host_target = - ]; then
        # echo "$filesystem doesn't have a target, won't replicate"
        # echo "Unlocking $filesystem"
        $zfs set $repl_lock=false $filesystem
    fi
    pool_target=`$zfs get -H $repl_dstpool $filesystem | cut -f3`
    if [ $pool_target = - ]; then
        #e cho "$filesystem doesn't have a target, won't replicate"
        # echo "Unlocking $filesystem"
        $zfs set $repl_lock=false $filesystem
    else
        # echo "$filesystem will replicated to $host_target"
        # echo -ne "                  Working on: $filesystem
        #          Prev Snap: $localprevsnap
        #          New Snap: $snap_name
        #          Sending to: $host_target
        #          On: $pool_target"
        # echo $localprevsnap
        # echo "Sending delta of $localprevsnap / $snap_name -> $repl_target"
        zfs_output=$($zfs send -cwvPi $localprevsnap $snap_name | /usr/bin/ssh $host_target $zfs recv -vFe $pool_target 2>&1)
        if [ $? -ne 0 ]; then
            $zfs destroy $snap_name
            $zfs set $repl_lock=false $filesystem
            slack_say "[$local_hostid] $zfs_output" High
        else
            $zfs set $repl_confirmed=true $snap_name
            $zfs set $repl_lock=false $filesystem
            slack_say "[$host_target] $zfs_output" HEALTHY

        fi
    fi
}


# Get it going
main
